CREATE TABLE public.products
(
    article integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    title character(15) COLLATE pg_catalog."default" NOT NULL,
    price integer NOT NULL,
    quantity integer,
    CONSTRAINT products_pkey PRIMARY KEY (article)
)

TABLESPACE pg_default;

ALTER TABLE public.products
    OWNER to postgres;

GRANT ALL(article) ON public.products TO postgres;

GRANT ALL(title) ON public.products TO postgres;

GRANT ALL(price) ON public.products TO postgres;


CREATE TABLE public.carts
(
    id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    name character(1) COLLATE pg_catalog."default" NOT NULL GENERATED ALWAYS AS (id) STORED,
    CONSTRAINT carts_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE public.carts
    OWNER to postgres;


CREATE TABLE public."cart contents"
(
    id_cart integer NOT NULL,
    article character(40) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT "cart contents_pkey" PRIMARY KEY (id_cart),
    CONSTRAINT id_carts FOREIGN KEY (id_cart)
        REFERENCES public.carts (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE public."cart contents"
    OWNER to postgres;
