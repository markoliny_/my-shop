package app.controllers;

import app.dao.ProductDaoImpl;
import app.model.Product;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(urlPatterns = "/")
public class ProductServlet extends HttpServlet {

    public ProductServlet(){
        super();
    }

    @Override
    protected void doGet(HttpServletRequest req,
                         HttpServletResponse resp) throws ServletException, IOException {

        ProductDaoImpl pdao = ProductDaoImpl.getInstance();
        ArrayList<Product> products = pdao.getAllProduct();
        req.setAttribute("products", products);

        RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/index.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req,
                          HttpServletResponse resp) throws ServletException, IOException {

        String items = req.getParameter("items");
        String article = req.getParameter("article");

        doGet(req, resp);
    }


}
