package app.controllers;

import app.model.Cart;
import app.model.Product;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet("/cart")
public class CartServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private ArrayList<Cart> cart = new ArrayList<Cart>();

    public CartServlet(){
        super();
    }

    @Override
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("jsp/cart.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {
        String command = request.getParameter("command");
        String article = request.getParameter("article");
        if (command.equals("buy")) {
            Product p = new Product(Integer.parseInt(article), "", 0, 0);
            addToCart(p);
            HttpSession session = request.getSession();

            System.out.println(cart.size());
            session.setAttribute("cart", cart);
            response.sendRedirect("jsp/cart.jsp");
        } else{
            if(command.equals("deleteCart")){
                Product p = new Product(Integer.parseInt(article), "", 0, 0);
                deleteFromCart(p);
                HttpSession session = request.getSession();

                System.out.println(cart.size());
                session.setAttribute("cart", cart);
                response.sendRedirect("/shop/cart.jsp");
            } else{
                if(command.equals("removeCart")){
                    Product p = new Product(Integer.parseInt(article), "", 0, 0);
                    removeFromCart(p);
                    HttpSession session = request.getSession();

                    session.setAttribute("cart", cart);
                    response.sendRedirect("/shop/cart.jsp");
                }else{
                    if(command.equals("setCart")){
                        Product p = new Product(Integer.parseInt(article), "", 0, 0);
                        setCart(p,Integer.parseInt((String) request.getParameter("soluong")));
                        HttpSession session = request.getSession();

                        session.setAttribute("cart", cart);
                        response.sendRedirect("/shop/cart.jsp");
                    }
                }
            }
        }
    }

    private String removeFromCart(Product p) {
        for (Cart item : cart) {
            if (item.getProduct().getArticle() == p.getArticle()) {
                cart.remove(item);
                return "cart";
            }
        }
        return "cart";
    }

    private String setCart(Product p, int s) {
        for (Cart item : cart) {
            if (item.getProduct().getArticle() == p.getArticle()) {
                item.setQuantity(s);
                return "cart";
            }
        }
        Cart c = new Cart();
        c.setProduct(p);
        c.setQuantity(s);
        cart.add(c);
        return "cart";
    }

    public String addToCart(Product p) {
        for (Cart item : cart) {
            if (item.getProduct().getArticle() == p.getArticle()) {
                item.setQuantity(item.getQuantity() + 1);
                return "cart";
            }
        }
        Cart c = new Cart();
        c.setProduct(p);
        c.setQuantity(1);
        cart.add(c);
        return "cart";
    }

    public String deleteFromCart(Product p) {
        for (Cart item : cart) {
            if (item.getProduct().getArticle() == p.getArticle() && item.getQuantity()>1) {
                item.setQuantity(item.getQuantity() - 1);
                return "cart";
            }
        }
        return "cart";
    }
}
