package app.model;

public class Product {
    private int article;
    private String title;
    private double price;
    private int quantity;

    public Product(int article, String title, double price, int quantity) {
        this.article = article;
        this.title = title;
        this.price = price;
        this.quantity = quantity;
    }

    public Product(String title, double price, int quantity) {
        this.title = title;
        this.price = price;
        this.quantity = quantity;
    }

    public Product(int article, int quantity) {
        this.article = article;
        this.quantity = quantity;
    }

    public int getArticle() {
        return article;
    }

    public void setArticle(int article) {
        this.article = article;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "Article" + article + "{" +
                "Title='" + title + '\'' +
                ", Price='" + price + '\''+
                ", Quantity='" + quantity +
                '}';
    }
}
