package app.model;

public class CartContents {

    private int id;
    private Product product;
    private Cart cart;
    private int quantity;

    public CartContents() {
    }

    public CartContents(int id, Cart cart, Product p, int quantity) {
        this.id = id;
        this.cart = cart;
        this.product = p;
        this.quantity = quantity;
    }

    public Cart getCart() {
        return cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
