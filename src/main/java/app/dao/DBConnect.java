package app.dao;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DBConnect {

    public String getPropertyValue(String propertyName){
        String propertyValue = "";
        Properties properties = new Properties();


        try {
            InputStream inputStream = this.getClass().getResourceAsStream("/application.properties");
            properties.load(inputStream);
            propertyValue = properties.getProperty(propertyName);
        } catch (IOException e) {
            System.out.println(e);
        }
        return propertyValue;
    }

    public static Connection getConnection() {
        DBConnect dbConnect = new DBConnect();
        Connection conn = null;
        try {
            Class.forName(dbConnect.getPropertyValue("DRIVER")); // Загружаем драйвер БД для ее взаимодействия с JVM
            conn = DriverManager.getConnection(dbConnect.getPropertyValue("URL"), dbConnect.getPropertyValue("USER"), dbConnect.getPropertyValue("PASSWORD"));

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return conn;
    }

    public static void main(String[] args) {
        Connection connection = DBConnect.getConnection();
    }
}
