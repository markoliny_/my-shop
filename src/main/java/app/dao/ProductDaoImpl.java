package app.dao;

import app.model.Product;
import java.sql.*;
import java.util.*;

public class ProductDaoImpl implements ProductDao {

    private PreparedStatement pstm = null;
    private static ProductDaoImpl instance = null;

    public ArrayList<Product> getAllProduct(){
        ArrayList<Product> products = new ArrayList<Product>();
        try {
            Connection conn = DBConnect.getConnection();
            pstm = conn.prepareStatement("SELECT * from products"); // Создаем объект для запроса к БД
            ResultSet resultSet = pstm.executeQuery();
            //Передаем через аргумент SQL запрос и в результате получаем все колонки(поля) из таблицы product
            while (resultSet.next()){
                int article = resultSet.getInt("article");
                String title = resultSet.getString("title");
                double price = resultSet.getDouble("price");
                int quantity = resultSet.getInt("quantity");
                Product product = new Product(article, title, price, quantity);
                products.add(product);
            }
            resultSet.close();
            pstm.close();
            conn.close();
        } catch (SQLException e) { e.printStackTrace(); }
        return products;
    }

    public int createProduct(Product product){
        return -1;
    }

    public int deleteProduct(int productId){
        return -1;
    }

    public int updateProduct(int article, Product product){
        try {
            Connection conn = DBConnect.getConnection();
            pstm = conn.prepareStatement("UPDATE product SET quantity=? WHERE article=?");
            pstm.setInt(4, product.getQuantity());
            pstm.setInt(1, article);
            pstm.executeUpdate();
            conn.close();
            pstm.close();
        } catch (SQLException e) { e.printStackTrace(); }
        return -1;
    }

    public Product findProductById(){
        return null;
    }

    public static ProductDaoImpl getInstance(){
        if (instance == null){
            instance = new ProductDaoImpl();
        }
        return instance;
    }

    public static void main(String[] args) {
        System.out.println(new ProductDaoImpl().getAllProduct());
    }
}
