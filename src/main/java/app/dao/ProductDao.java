package app.dao;

import app.model.Product;

import java.util.ArrayList;
import java.util.List;

public interface ProductDao {
    ProductDao INSTANCE_PRODUCT = new ProductDaoImpl();

    ArrayList<Product> getAllProduct();

}
