<%@ page language = "java" contentType= "text/html; charset=UTF-8"
        pageEncoding="UTF-8"%>
<%@ page import="app.dao.*, app.model.*, java.text.NumberFormat, java.util.ArrayList" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>iShop</title>
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    </head>
    <body class="w3-light-grey">
        <button# id="myBtn">Корзина</button>
        <div id="myCart"> class="cart">
            <div class="cart-content">
                 <span class="close">$times;</span>
                 <p>Содержимое корзины</p>
            </div>
        </div>

        <script src="script.js"></script>

        <div class="w3-container w3-center w3-gold  w3-right-align">
            <h1>Cart</h1>
        </div>

        <div class="w3-container w3-center w3-margin-bottom w3-padding">
            <div class="w3-card-4">
                <div class="w3-container w3-light-blue">
                    <h2>Products in cart</h2>
                </div>
    <ul class=\"w3-ul\">
        <table cellpadding="3" cellspacing="6">
          <tr>
            <th>Код</th>
            <th>Наименование</th>
            <th>Цена, 1шт.</th>
            <th>Количество</th>
          </tr>
				<%
					ProductDAOImpl productDAO = new ProductDAOImpl();

									NumberFormat nf = NumberFormat.getInstance();
									nf.setMinimumIntegerDigits(0);
									double total = 0;
									ArrayList<Cart> cart=null;
									if(session.getAttribute("cart")!=null){
									cart = (ArrayList<Cart>) session
											.getAttribute("cart");}
				%>
				<%
					if (cart != null) {
										for (Cart c : cart) {
											total = total
													+ (c.getQuantity() * productDAO.getProduct(
															c.getProduct().getMa_san_pham()).getGia_ban());
				%>
               <tr>
               <td><%= p.getArticle() %></td>
               <td><%= p.getTitle() %></td>
               <td><%= p.getPrice() %></td>
               <td>
                 <form method="post" class="w3-selection w3-padding">
                      <a class='minus'>-</a>
                      <input type="text" class="num" name="quantity" value="<%= p.getQuantity() %>" size="1">
                      <a class='plus'>+</a>
                </td>
                <td>
                    <button  class="w3-btn w3-container w3-dark-grey w3-hover-yellow w3-round-large" onclick="location.href=">Buy</button>
                </from>
                </td>
                </tr>
               <%
                }
             %>
        </table>
    </ul>
  </div>
</div>
</body>
</html>