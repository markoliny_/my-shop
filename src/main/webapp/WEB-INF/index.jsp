<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="app.dao.*, app.model.*, java.util.ArrayList" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>iShop</title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <style>
        .cart {
            max-width: 500px;
            margin: 0 auto;
            font-family: Arial;
        }

        .btnCart {
            position: absolute;
            top: 5px;
            right: 5px;
            display: block;
            width: 220px;
            text-align: center;
            text-transform: uppercase;
            color: black;
            border: 1px solid mediumturquoise;
            padding: 15px;
            margin: 20px;
            cursor: pointer;
        }

        .btn {
            display: block;
            width: 160px;
            text-align: center;
            text-transform: uppercase;
            color: black;
            border: 1px solid mediumturquoise;
            padding: 15px;
            margin: 20px;
            cursor: pointer;
        }

        .popup {
            display: block;
            margin: 0 auto;
            color: black;
            border: 1px solid mediumturquoise;
            padding: 5px;
            position: absolute;
            transition: .5s all;
            top: -999px;
        }

        #modal {
            display: none;
        }

        #modal:checked ~ .popup {
            top: 0;
        }

        .closes {
            position: absolute;
            top: 5px;
            right: 10px;
            cursor: pointer;
            font-size: 25px;
        }
    </style>
</head>
<body class="w3-light-grey">
<div class="w3-container w3-center w3-gold  w3-right-align">
    <h1>iShop</h1>
</div>
<div class="cart">
    <input type="checkbox" id="modal">
    <label class="btnCart w3-light-grey" for="modal">
        <h5>Корзина
        </h5>
        <% ArrayList<Product> prod = (ArrayList<Product>) request.getAttribute("products"); %>
        <h6>Товаров в корзине: <%= prod.size() %>
        </h6>
        <h6>Сумма к оплате: 8231
        </h6>
    </label>
    <form action="" class="popup w3-light-grey">
        <ul class=\"w3-ul\">
            <table cellpadding="3" cellspacing="3">
                <tr>
                    <th>Наименование</th>
                    <th>Кол-во</th>
                    <th>Сумма</th>
                </tr>
                <% for (Product p : prod) { %>
                <tr>
                    <td><%= p.getTitle() %>
                    </td>
                    <td><%= p.getQuantity() %>
                    </td>
                    <td><%= p.getPrice() * p.getQuantity() %>
                    </td>
                    <td>
                        <button class="w3-btn w3-container w3-dark-grey w3-hover-yellow w3-round-large"
                                href="CartServlet?command=buy&article=<%= p.getArticle() %>">Удалить
                        </button>
                    </td>
                </tr>
                <% } %>
            </table>
        </ul>
        <label class="closes" for="modal">&times;</label>
        <button class="btn w3-container w3-light-blue w3-hover-yellow w3-round-large"
                href="CartServlet?command=buy">Оформить
        </button>
    </form>
</div>
<div class="w3-container w3-center w3-margin-bottom w3-padding">
    <div class="w3-card-4">
        <div class="w3-container w3-light-blue">
            <h2>Товары</h2>
        </div>
        <ul class=\"w3-ul\">
            <table cellpadding="3" cellspacing="4">
                <tr>
                    <th>Код</th>
                    <th>Наименование</th>
                    <th>Цена, $</th>
                    <th>Количество</th>
                </tr>
                <% ArrayList<Product> products = (ArrayList<Product>) request.getAttribute("products"); %>
                <% for (Product p : products) { %>
                <tr>
                    <td><%= p.getArticle() %>
                    </td>
                    <td><%= p.getTitle() %>
                    </td>
                    <td><%= p.getPrice() %>
                    </td>
                    <td>
                        <a class='minus'>-</a>
                        <input type="text" class="num" name="quantity" value="<%= p.getQuantity() %>" size="1">
                        <a class='plus'>+</a>
                    </td>
                    <td>
                        <button class="w3-btn w3-container w3-dark-grey w3-hover-yellow w3-round-large"
                                href="CartServlet?command=buy&article=<%= p.getArticle() %>">Добавить
                        </button>
                    </td>
                </tr>
                <% } %>
            </table>
        </ul>
    </div>
</div>
</body>
</html>

